#!/bin/sh

sudo apt update
sudo apt install nginx
systemctl status nginx

sudo apt install nano
sudo apt install git

sudo mkdir -p /var/www/gachi_huyachi
sudo mkdir -p /var/www/gachi_huyachi/jar
sudo chmod 777 /var/www/gachi_huyachi/jar


#____________nginx________________

sudo tee /etc/nginx/sites-available/gachi_huyachi <<EOF
server {
    listen 80;
    listen [::]:80;

    server_name gachi-huyachi.fun www.gachi-huyachi.fun;

    location / {
        proxy_pass http://127.0.0.1:8081;
        proxy_set_header Host \$host;
        proxy_set_header X-Real-IP \$remote_addr;
        proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto \$scheme;
    }
}
EOF

sudo ln -s /etc/nginx/sites-available/gachi_huyachi /etc/nginx/sites-enabled/
sudo nginx -t
sudo systemctl restart nginx


#________________JDK___18______________________

sudo apt update
sudo apt install -y curl wget
curl -O https://download.java.net/java/GA/jdk18/43f95e8614114aeaa8e8a5fcf20a682d/36/GPL/openjdk-18_linux-x64_bin.tar.gz
tar xvf openjdk-18_linux-x64_bin.tar.gz
sudo mv jdk-18 /opt/
rm -rf openjdk-18_linux-x64_bin.tar.gz

sudo tee /etc/profile.d/jdk18.sh <<EOF
export JAVA_HOME=/opt/jdk-18
export PATH=\$PATH:\$JAVA_HOME/bin
EOF

source /etc/profile.d/jdk18.sh
echo $JAVA_HOME
java -version


#_______________GITLAB_RUNNER__________________________
sudo groupadd runner
useradd -m -d /home/gitlab-deployer gitlab-deployer
usermod -a -G runner gitlab-deployer

passwd gitlab-deployer

sudo wget -O /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64
sudo chmod +x /usr/local/bin/gitlab-runner
ln -s /usr/local/bin/gitlab-runner /etc/alternatives/gitlab-runner
ln -s /etc/alternatives/gitlab-runner /usr/bin/gitlab-runner

# Переходим на сайт gitlab.com -> deploy-project -> Settings -> CI/CD -> Runners -> Specific Runners и копируем registration token
gitlab-runner register --config /etc/gitlab-runner/gitlab-deployer-config.toml


sudo tee /etc/systemd/system/gitlab-deployer.service <<EOF
[Unit]
Description=GitLab Deploy Runner
After=syslog.target network.target
ConditionFileIsExecutable=/usr/local/bin/gitlab-runner
[Service]
StartLimitInterval=5
StartLimitBurst=10
ExecStart=/usr/local/bin/gitlab-runner "run" "--working-directory" "/home/gitlab-deployer" "--config" "/etc/gitlab-runner/gitlab-deployer-config.toml" "--service" "gitlab-deployer" "--syslog" "--user" "gitlab-deployer"
Restart=always
RestartSec=120
[Install]
WantedBy=multi-user.target
EOF


ln -s /var/www/gachi_huyachi/jar /home/gitlab-deployer/deploy
chmod 777 /home/gitlab-deployer/deploy
rm -rf /home/gitlab-deployer/.bash_logout


systemctl enable gitlab-deployer.service
systemctl start gitlab-deployer.service
systemctl status gitlab-deployer.service


visudo
gitlab-deployer ALL=NOPASSWD: /usr/bin/systemctl status gachi_huyachi.service, /usr/bin/systemctl start gachi_huyachi.service, /usr/bin/systemctl restart gachi_huyachi.service


#_______________JAVA_BACK_SERVICE__________________________

sudo tee /var/www/gachi_huyachi/run.sh <<EOF
#!/bin/bash
/opt/jdk-18/bin/java \\
  -Dserver.port=8081 \\
  -Dlog_path=/var/www/gachi_huyachi/log/app.log \\
  -Dfiles_directory=/var/www/gachi_huyachi/files \\
  -jar /var/www/gachi_huyachi/jar/gachiHuyachi.jar
EOF


# сервис для бека, который раннер будет перезапускать
sudo tee /etc/systemd/system/gachi_huyachi.service <<EOF
[Unit]
Description=gachi_huyachi_backend
After=syslog.target network.target
# ConditionFileIsExecutable=/usr/local/bin/gitlab-runner
[Service]
ExecStart=/bin/bash /var/www/gachi_huyachi/run.sh
ExecStop=/bin/kill -HUP \$MAINPID
Restart=always
# Набор переменных окружений
# Environment=TELEGRAM_TOKEN=Foo
EOF

chmod 774 /var/www/gachi_huyachi/run.sh

systemctl enable gachi_huyachi.service
systemctl start gachi_huyachi.service
systemctl status gachi_huyachi.service

journalctl -u gachi_huyachi.service

#_______________SSL__________________________

scp DD2E78C5BFD87F5401236C93C0AB2AA7.txt root@78.24.222.87:/var/www/gachi_huyachi

#добавляем строки
sudo tee /etc/nginx/sites-available/gachi_huyachi <<EOF
server {
    listen 80;
    listen [::]:80;

    server_name gachi-huyachi.fun www.gachi-huyachi.fun;
    root /etc/nginx/sites-available/gachi_huyachi;

    location /.well-known/pki-validation {
        add_header Content-disposition "attachment; filename=DD2E78C5BFD87F5401236C93C0AB2AA7.txt";
        index DD2E78C5BFD87F5401236C93C0AB2AA7.txt;
        alias /var/www/gachi_huyachi;
    }
}
EOF

sudo systemctl restart nginx

# проходим верификацию https://manage.sslforfree.com/certificate/install/fb36219c965c2fd1f152981fba9c9d35
# скачиваем сертификаты

scp ca_bundle.crt root@78.24.222.87:/etc/nginx/certs/
scp certificate.crt root@78.24.222.87:/etc/nginx/certs/
scp private.key root@78.24.222.87:/etc/nginx/certs/


sudo ln -s /etc/nginx/sites-available/ssoraka_ru /etc/nginx/sites-enabled/
sudo nano /etc/nginx/nginx.conf
sudo nginx -t
sudo systemctl restart nginx

sudo tee /etc/nginx/sites-available/gachi_huyachi <<EOF
server {
    listen 80;
    listen [::]:80;

    server_name gachi-huyachi.fun www.gachi-huyachi.fun;

    location /gs-guide-websocket {
        proxy_pass http://localhost:8081;  # Укажите ваш адрес и порт
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";
        proxy_set_header Host $host;
    }


    location / {
        proxy_pass http://127.0.0.1:8081;
        proxy_http_version 1.1;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
       # proxy_set_header X-Forwarded-Proto https;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";
    }
}

server {
    listen 443 ssl;
    listen [::]:443 ssl;

    server_name gachi-huyachi.fun www.gachi-huyachi.fun;
    root /etc/nginx/sites-available/gachi_huyachi;

#       ssl                  on;
    ssl_certificate      /etc/nginx/certs/certificate.crt;
    ssl_certificate_key  /etc/nginx/certs/private.key;

    ssl_prefer_server_ciphers on;
    ssl_session_timeout 5m;
    ssl_session_cache shared:SSL:50m;
    ssl_session_tickets off;

#       ssl_ciphers          HIGH:!aNULL:!MD5:!3DES;
#       ssl_ciphers          ALL:EECDH+aRSA+AESGCM:EDH+aRSA+AESGCM:EECDH+aRSA+AES:EDH+aRSA+AES;
    ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
    ssl_ciphers 'ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-DSS-AES128-GCM-SHA256:kEDH+AESGCM:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA:ECDHE-ECDSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-DSS-AES128-SHA256:DHE-RSA-AES256-SHA256:DHE-DSS-AES256-SHA:DHE-RSA-AES256-SHA:ECDHE-RSA-DES-CBC3-SHA:ECDHE-ECDSA-DES-CBC3-SHA:AES128-GCM-SHA256:AES256-GCM-SHA384:AES128-SHA256:AES256-SHA256:AES128-SHA:AES256-SHA:AES:CAMELLIA:DES-CBC3-SHA:!aNULL:!eNULL:!EXPORT:!DES:!RC4:!MD5:!PSK:!aECDH:!EDH-DSS-DES-CBC3-SHA:!EDH-RSA-DES-CBC3-SHA:!KRB5-DES-CBC3-SHA';

    location /gs-guide-websocket {
        proxy_pass http://localhost:8081;  # Укажите ваш адрес и порт
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";
        proxy_set_header Host $host;
    }

    location / {
        proxy_pass http://127.0.0.1:8081;
        proxy_http_version 1.1;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
       # proxy_set_header X-Forwarded-Proto https;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";
    }
}

EOF

sudo nginx -t
sudo systemctl restart nginx




#http {
#    map $http_upgrade $connection_upgrade {
#        default upgrade;
#        ''      close;
#    }
#
#    server {
#        listen 80;
#        listen [::]:80;
#
#        server_name gachi-huyachi.fun www.gachi-huyachi.fun;
#        root /etc/nginx/sites-available/gachi_huyachi;
#
#        location /gs-guide-websocket {
#            proxy_pass http://127.0.0.1:8081;
#            proxy_http_version 1.1;
#            proxy_set_header Upgrade $http_upgrade;
#            proxy_set_header Connection "upgrade";
#        }
#
#        location / {
#            proxy_pass http://127.0.0.1:8081;
#            proxy_http_version 1.1;
#            proxy_set_header Upgrade $http_upgrade;
#            proxy_set_header Connection "close";
#            proxy_set_header Host $host;
#            proxy_set_header X-Real-IP $remote_addr;
#            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
#            proxy_set_header X-Forwarded-Proto https;
#        }
#    }
#
#    server {
#        listen 443 ssl;
#        listen [::]:443 ssl;
#
#        server_name gachi-huyachi.fun www.gachi-huyachi.fun;
#        root /etc/nginx/sites-available/gachi_huyachi;
#
#        ssl_certificate      /etc/nginx/certs/certificate.crt;
#        ssl_certificate_key  /etc/nginx/certs/private.key;
#
#        location /gs-guide-websocket {
#            proxy_pass http://127.0.0.1:8081;
#            proxy_http_version 1.1;
#            proxy_set_header Upgrade $http_upgrade;
#            proxy_set_header Connection "upgrade";
#            proxy_set_header Host $host;
#        }
#
#        location / {
#            proxy_pass http://127.0.0.1:8081;
#            proxy_http_version 1.1;
#            proxy_set_header Upgrade $http_upgrade;
#            proxy_set_header Connection "close";
#            proxy_set_header Host $host;
#            proxy_set_header X-Real-IP $remote_addr;
#            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
#            proxy_set_header X-Forwarded-Proto https;
#        }
#    }
#}