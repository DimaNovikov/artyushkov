package ru.artyashkov.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class FileMessageDto {
    private String id;
    private String name;
    private String senderName;
    private String receiverName;
    private String created;
    private String c1;
    private String c2;
}
