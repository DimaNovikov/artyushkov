package ru.artyashkov.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ConnectionDto {
    private String c1;
    private String c2;
}
