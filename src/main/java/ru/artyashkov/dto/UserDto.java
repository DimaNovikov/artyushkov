package ru.artyashkov.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UserDto {
    private String name;
    private String publicKey;
    private String g;
    private String p;
}
