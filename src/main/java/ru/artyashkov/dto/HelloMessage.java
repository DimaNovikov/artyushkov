package ru.artyashkov.dto;

import lombok.Data;

@Data
public class HelloMessage {
    private String receiver;
    private String fileName;
}
