package ru.artyashkov.mapper;

import org.springframework.stereotype.Component;
import ru.artyashkov.algorithms.model.EncryptedSecretKey;
import ru.artyashkov.model.EncryptedSecretKeyEntity;

@Component
public class EncryptedSecretKeyMapper {
    public EncryptedSecretKey toKey(EncryptedSecretKeyEntity key) {
        return EncryptedSecretKey.builder()
                .c1(key.getC1())
                .c2(key.getC2())
                .build();
    }

    public EncryptedSecretKeyEntity toKeyEntity(EncryptedSecretKey key) {
        return EncryptedSecretKeyEntity.builder()
                .c1(key.getC1())
                .c2(key.getC2())
                .build();
    }
}
