package ru.artyashkov.mapper;

import org.springframework.stereotype.Component;
import ru.artyashkov.algorithms.model.Keys;
import ru.artyashkov.model.KeysEntity;

@Component
public class KeysMapper {
    public Keys toKeys(KeysEntity keys) {
        return Keys.builder()
                .privateKey(keys.getPrivateKey())
                .publicKey(keys.getPublicKey())
                .g(keys.getG())
                .p(keys.getP())
                .build();
    }

    public KeysEntity toKeysEntity(Keys keys) {
        return KeysEntity.builder()
                .privateKey(keys.getPrivateKey())
                .publicKey(keys.getPublicKey())
                .g(keys.getG())
                .p(keys.getP())
                .build();
    }
}
