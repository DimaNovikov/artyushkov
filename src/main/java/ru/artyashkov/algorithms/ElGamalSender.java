package ru.artyashkov.algorithms;

import ru.artyashkov.algorithms.model.EncryptedSecretKey;
import ru.artyashkov.algorithms.model.Keys;

import java.math.BigInteger;
import java.security.SecureRandom;
public class ElGamalSender {
    private BigInteger p;
    private BigInteger g;
    private BigInteger publicKey;
    public ElGamalSender(Keys keys) {
        this.p = keys.getP();
        this.g = keys.getG();
        this.publicKey = keys.getPublicKey();
    }
    public EncryptedSecretKey encryptSecretKey(BigInteger secretKey) {
        try {
            BigInteger k = new BigInteger(p.bitLength() - 1, new SecureRandom());
            BigInteger C1 =  g.modPow(k, p);
            BigInteger C2 = publicKey.modPow(k, p).multiply(secretKey).mod(p);
            return EncryptedSecretKey.builder()
                    .c1(C1)
                    .c2(C2)
                    .build();
        } catch (RuntimeException e) {
            e.printStackTrace();
            throw new RuntimeException();
        }
    }
}




