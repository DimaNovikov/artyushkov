package ru.artyashkov.algorithms.model;

import lombok.Builder;
import lombok.Data;

import java.math.BigInteger;

@Data
@Builder
public class EncryptedSecretKey {
    private BigInteger c1;
    private BigInteger c2;
}
