package ru.artyashkov.algorithms.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.artyashkov.algorithms.PrimeTests;

import java.math.BigInteger;
import java.security.SecureRandom;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Keys {
    private static final int BIT_LENGTH = 2048;
    private BigInteger privateKey;
    private BigInteger p;
    private BigInteger g;
    private BigInteger publicKey;

    public void init() {
        do {
            p = BigInteger.probablePrime(BIT_LENGTH, new SecureRandom());
        } while (!PrimeTests.isPrime(p));
        privateKey = new BigInteger(p.bitLength() - 1, new SecureRandom());
        g = new BigInteger("2");
        publicKey = g.modPow(privateKey, p);
    }
}
