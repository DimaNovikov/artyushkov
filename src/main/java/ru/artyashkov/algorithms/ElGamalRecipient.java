package ru.artyashkov.algorithms;

import ru.artyashkov.algorithms.model.EncryptedSecretKey;
import ru.artyashkov.algorithms.model.Keys;

import java.math.BigInteger;

public class ElGamalRecipient {
    private final BigInteger privateKey;
    private BigInteger p;
    private BigInteger g;
    private BigInteger publicKey;
    private int bitLength = 2048;
    public ElGamalRecipient(Keys keys) {
        privateKey = keys.getPrivateKey();
        p = keys.getP();
        g = keys.getG();
        publicKey = keys.getPublicKey();
    }
    public BigInteger decryptSecretKey(EncryptedSecretKey key) {
        try {
            BigInteger C1 = key.getC1();
            BigInteger C2 = key.getC2();
            BigInteger K = C1.modPow(privateKey, p).modInverse(p);
            BigInteger decodedSecretKey = C2.multiply(K).mod(p);
            return decodedSecretKey;
        } catch (RuntimeException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
}
