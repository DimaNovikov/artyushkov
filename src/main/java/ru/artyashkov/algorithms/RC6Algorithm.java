package ru.artyashkov.algorithms;

import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.security.SecureRandom;
import java.util.Arrays;

public class RC6Algorithm {
    private static final int blockSize = 16; // 128 bits
    private static final int rounds = 26;
    private int[] roundKeys;
    private BigInteger secretKey;
    private final int secretKeySize = 128;

    public RC6Algorithm() {
        this.roundKeys = null;
        this.secretKey = null;
    }
    public void generateSecretKey() {
        if (this.secretKey == null) {
            do {
                this.secretKey = new BigInteger(this.secretKeySize, new SecureRandom());
            } while (this.secretKey.toByteArray().length != 16);
            expandKey(this.secretKey);
        }
    }

    public BigInteger getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(BigInteger secretKey) {
        if (this.secretKey == null) {
            this.secretKey = secretKey;
            expandKey(secretKey);
        }
    }

    private void expandKey(BigInteger keyInBig) {
        byte [] secretKey = keyInBig.toByteArray();
        int[] key = new int[(secretKey.length + 3) / 4];
            for (int i = 0, offset = 0; i < key.length; ++i, offset += 4) {
                key[i] = ByteBuffer.wrap(secretKey, offset, 4).getInt();
            }
        int[] expandedKey = new int[2 * rounds + 4];
        expandedKey[0] = 0xB7E15163;
        for (int i = 1; i < expandedKey.length; i++) {
            expandedKey[i] = expandedKey[i - 1] + 0x9E3779B9;
        }
        int a = 0, b = 0;
        int i = 0, j = 0;
        int v = 3 * Math.max(key.length, expandedKey.length);
        for (int k = 0; k < v; ++k) {
            a = expandedKey[i] = rotateLeft(expandedKey[i] + a + b, 3);
            b = key[j] = rotateLeft(key[j] + a + b, a + b);
            i = (i + 1) % expandedKey.length;
            j = (j + 1) % key.length;
        }
        roundKeys = expandedKey;
    }

    public byte[] encrypt(byte[] data) {
        int numBlocks = (data.length + blockSize - 1) / blockSize;
        byte[] cipherText = new byte[numBlocks * blockSize];
        for (int block = 0; block < numBlocks; block++) {
            int blockOffset = block * blockSize;
            byte[] blockBytes = Arrays.copyOfRange(data, blockOffset, blockOffset + blockSize);
            int[] words = bytesToWords(blockBytes);
            encryptBlock(words);
            byte[] encryptedBlock = wordsToBytes(words);
            System.arraycopy(encryptedBlock, 0, cipherText, blockOffset, blockSize);
        }
       return cipherText;
    }
    private void encryptBlock(int[] words) {
        int a = words[0], b = words[1], c = words[2], d = words[3];
        b += roundKeys[0];
        d += roundKeys[1];
        for (int i = 1; i <= rounds; i++) {
            int t, u;
            t = rotateLeft(b * (2 * b + 1), 5);
            u = rotateLeft(d * (2 * d + 1), 5);
            a = rotateLeft(a ^ t, u) + roundKeys[2 * i];
            c = rotateLeft(c ^ u, t) + roundKeys[2 * i + 1];
            int temp = a;
            a = b;
            b = c;
            c = d;
            d = temp;
        }
        a += roundKeys[2 * rounds + 2];
        c += roundKeys[2 * rounds + 3];
        words[0] = a;
        words[1] = b;
        words[2] = c;
        words[3] = d;
    }

    public byte[] decrypt(byte[] data) {
        int numBlocks = data.length / blockSize;
        byte[] decryptedText = new byte[data.length];
        for (int block = 0; block < numBlocks; block++) {
            int blockOffset = block * blockSize;
            byte[] blockBytes = Arrays.copyOfRange(data, blockOffset, blockOffset + blockSize);
            int[] blockWords = bytesToWords(blockBytes);
            decryptBlock(blockWords);
            byte[] decryptedBlock = wordsToBytes(blockWords);
            System.arraycopy(decryptedBlock, 0, decryptedText, blockOffset, blockSize);
        }
       return removePadding(decryptedText);
    }

    private void decryptBlock(int[] words) {
        int a = words[0], b = words[1], c = words[2], d = words[3];
        c -= roundKeys[2 * rounds + 3];
        a -= roundKeys[2 * rounds + 2];
        for (int i = rounds; i >= 1; i--) {
            int temp = d;
            d = c;
            c = b;
            b = a;
            a = temp;
            int u = rotateLeft(d * (2 * d + 1), 5);
            int t = rotateLeft(b * (2 * b + 1), 5);
            c = rotateRight(c - roundKeys[2 * i + 1], t) ^ u;
            a = rotateRight(a - roundKeys[2 * i], u) ^ t;
        }
        d -= roundKeys[1];
        b -= roundKeys[0];
        words[0] = a;
        words[1] = b;
        words[2] = c;
        words[3] = d;
    }

    private byte[] removePadding(byte[] plaintext) {
        int paddingLength = plaintext[plaintext.length - 1];

        //TODO разобраться, почему паддинг не работает
        if (paddingLength == 0) {
            int i = plaintext.length;
            while (i >= 0 && plaintext[i - 1] == 0) {
                i--;
            }
            return Arrays.copyOfRange(plaintext, 0, i);
        }

        return Arrays.copyOfRange(plaintext, 0, plaintext.length - paddingLength);
    }


    private int[] bytesToWords(byte[] bytes) {
        int[] words = new int[bytes.length / 4];
        for (int i = 0, offset = 0; i < words.length; i++, offset += 4) {
            words[i] = ByteBuffer.wrap(bytes, offset, 4).getInt();
        }
        return words;
    }
    private byte[] wordsToBytes(int[] words) {
        byte[] bytes = new byte[words.length * 4];
        for (int i = 0, offset = 0; i < words.length; i++, offset += 4) {
            ByteBuffer.wrap(bytes, offset, 4).putInt(words[i]);
        }
        return bytes;
    }

    private int rotateLeft(int value, int shift) { return (value << shift) | (value >>> (32 - shift)); }
    private int rotateRight(int value, int shift) { return (value >>> shift) | (value << (32 - shift)); }

}
