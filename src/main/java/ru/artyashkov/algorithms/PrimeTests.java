package ru.artyashkov.algorithms;

import java.math.BigInteger;
import java.util.Random;

public class PrimeTests {
    public static boolean isPrime(BigInteger number) {
        if (number.compareTo(BigInteger.ONE) <= 0) {
            return false;
        }
        if (number.compareTo(BigInteger.valueOf(3)) <= 0) {
            return true;
        }
        if (number.mod(BigInteger.valueOf(2)).equals(BigInteger.ZERO)) {
            return false;
        }
        return testSoloveyStrassen(number) && testMillerRabin(number) && testFerma(number);
    }
    private static boolean testSoloveyStrassen(BigInteger n) {
        for (int i = 0; i < 100; i++) {
            BigInteger a = randomBigInteger(BigInteger.TWO, n.subtract(BigInteger.ONE));
            BigInteger x = jacobiSymbol(a, n).mod(n);
            BigInteger y = a.modPow(n.subtract(BigInteger.ONE).divide(BigInteger.valueOf(2)), n);
            if (!x.equals(y)) {
                return false;
            }
        }
        return true;
    }
    private static boolean testMillerRabin(BigInteger n) {
        BigInteger r = BigInteger.ZERO;
        BigInteger d = n.subtract(BigInteger.ONE);
        while (d.mod(BigInteger.valueOf(2)).equals(BigInteger.ZERO)) {
            d = d.divide(BigInteger.valueOf(2));
            r = r.add(BigInteger.ONE);
        }
        for (int i = 0; i < 100; i++) {
            BigInteger a = randomBigInteger(BigInteger.TWO, n.subtract(BigInteger.TWO));
            BigInteger x = a.modPow(d, n);
            if (x.equals(BigInteger.ONE) || x.equals(n.subtract(BigInteger.ONE))) {
                continue;
            }
            boolean isWitness = false;
            for (BigInteger j = BigInteger.ONE; j.compareTo(r) < 0; j = j.add(BigInteger.ONE)) {
                x = x.modPow(BigInteger.valueOf(2), n);
                if (x.equals(BigInteger.ONE)) {
                    return false; // n - составное
                }
                if (x.equals(n.subtract(BigInteger.ONE))) {
                    isWitness = true;
                    break;
                }
            }
            if (!isWitness) {
                return false; // n - составное
            }
        }
        return true; // n - вероятно простое
    }
    private static boolean testFerma(BigInteger n) {
        for (int i = 0; i < 100; i++) {
            BigInteger a = randomBigInteger(BigInteger.TWO, n.subtract(BigInteger.ONE));
            BigInteger result = a.modPow(n.subtract(BigInteger.ONE), n);
            if (!result.equals(BigInteger.ONE)) {
                return false; // n - составное
            }
        }
        return true; // n - вероятно простое
    }
    private static BigInteger jacobiSymbol(BigInteger a, BigInteger n) {
        if (a.equals(BigInteger.ZERO)) {
            return BigInteger.ZERO;
        }
        if (a.equals(BigInteger.ONE)) {
            return BigInteger.ONE;
        }
        BigInteger two = BigInteger.valueOf(2);
        int e = 0;
        BigInteger a1 = a;
        while (a1.mod(two).equals(BigInteger.ZERO)) {
            a1 = a1.divide(two);
            e++;
        }
        BigInteger s;
        if (e % 2 == 0) {
            s = BigInteger.ONE;
        } else {
            if (n.mod(BigInteger.valueOf(8)).equals(BigInteger.ONE) || n.mod(BigInteger.valueOf(8)).equals(BigInteger.valueOf(7))) {
                s = BigInteger.ONE;
            } else if (n.mod(BigInteger.valueOf(8)).equals(BigInteger.valueOf(3)) || n.mod(BigInteger.valueOf(8)).equals(BigInteger.valueOf(5))) {
                s = BigInteger.valueOf(-1);
            } else {
                s = BigInteger.ONE;
            }
        }
        if (a1.equals(BigInteger.ONE)) {
            return s;
        }
        if (n.mod(BigInteger.valueOf(4)).equals(BigInteger.valueOf(3)) && a1.mod(BigInteger.valueOf(4)).equals(BigInteger.valueOf(3))) {
            s = s.negate();
        }
        return s.multiply(jacobiSymbol(n.mod(a1), a1));
    }
    private static BigInteger randomBigInteger(BigInteger min, BigInteger max) {
        BigInteger range = max.subtract(min);
        int bits = range.bitLength();
        Random random = new Random();
        BigInteger result;
        do {
            result = new BigInteger(bits, random);
        } while (result.compareTo(range) > 0);
        return result.add(min);
    }
    public static void main(String[] args) {
        BigInteger number1 = new BigInteger("10657331232548839");
        BigInteger number2 = new BigInteger("193612036600479");
        BigInteger numberToTest = number1.multiply(number2); // Замените на число, которое вы хотите проверить на простоту

        if (isPrime(numberToTest)) {
            System.out.println(numberToTest + " является простым числом.");
        } else {
            System.out.println(numberToTest + " не является простым числом.");
        }
    }
}
