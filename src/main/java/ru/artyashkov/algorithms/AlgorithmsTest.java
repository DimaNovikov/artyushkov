package ru.artyashkov.algorithms;


import ru.artyashkov.algorithms.model.Keys;

public class AlgorithmsTest {
    public static void main(String[] args) {
        Keys user = new Keys();
        user.init();

        ElGamalSender sender = new ElGamalSender(user);
        ElGamalRecipient recipient = new ElGamalRecipient(user);
        RC6Algorithm encryptMachine = new RC6Algorithm();
        RC6Algorithm decryptMachine = new RC6Algorithm();

        encryptMachine.generateSecretKey();
        decryptMachine.setSecretKey(recipient.decryptSecretKey(sender.encryptSecretKey(encryptMachine.getSecretKey())));

        String openText = "This is a test open text";
        byte[] encryptedText = encryptMachine.encrypt(openText.getBytes());
        byte[] decryptedText = decryptMachine.decrypt(encryptedText);
        System.out.println(new String(decryptedText));

    }

}