package ru.artyashkov.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.artyashkov.convert.BigIntegerConverter;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Embeddable;
import java.math.BigInteger;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Embeddable
public class KeysEntity {

    @Convert(converter = BigIntegerConverter.class)
    @Column(name = "private_key")
    private BigInteger privateKey;
    @Convert(converter = BigIntegerConverter.class)
    @Column(name = "public_key")
    private BigInteger publicKey;
    @Convert(converter = BigIntegerConverter.class)
    @Column(name = "p")
    private BigInteger p;
    @Convert(converter = BigIntegerConverter.class)
    @Column(name = "g")
    private BigInteger g;
}
