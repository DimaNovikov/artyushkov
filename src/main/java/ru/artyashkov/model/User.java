package ru.artyashkov.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Data
@NoArgsConstructor
@SuperBuilder
@Entity
@Table(name = "users")
public class User extends BaseEntity {

    @Column(name = "name")
    private String name;

    @Embedded
    private KeysEntity keys;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @OneToMany(targetEntity = Connection.class, mappedBy = "sender", cascade = CascadeType.ALL)
    private Set<Connection> sender = new HashSet<>();

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @OneToMany(targetEntity = Connection.class, mappedBy = "receiver", cascade = CascadeType.ALL)
    private Set<Connection> receiver = new HashSet<>();
}
