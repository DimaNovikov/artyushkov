package ru.artyashkov.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.artyashkov.convert.BigIntegerConverter;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Embeddable;
import java.math.BigInteger;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Embeddable
public class EncryptedSecretKeyEntity {

    @Convert(converter = BigIntegerConverter.class)
    @Column(name = "c1")
    private BigInteger c1;
    @Convert(converter = BigIntegerConverter.class)
    @Column(name = "c2")
    private BigInteger c2;
}
