package ru.artyashkov.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.artyashkov.model.User;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByName(String name);
}
