package ru.artyashkov.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import ru.artyashkov.model.Connection;
import ru.artyashkov.model.User;

public interface ConnectionRepository extends JpaRepository<Connection, Long> {
    Connection findBySenderAndReceiver(@Param("sender") User sender, @Param("receiver") User receiver);
}
