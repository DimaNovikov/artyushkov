package ru.artyashkov.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ru.artyashkov.model.FileMessage;
import ru.artyashkov.model.User;

import java.util.List;

public interface FileMessageRepository extends JpaRepository<FileMessage, Long> {

    FileMessage findByPath(String path);
}
