package ru.artyashkov.convert;

import javax.persistence.AttributeConverter;
import java.math.BigInteger;

public class BigIntegerConverter implements AttributeConverter<BigInteger, String> {

    @Override
    public String convertToDatabaseColumn(BigInteger bigInteger) {
        return bigInteger.toString();
    }

    @Override
    public BigInteger convertToEntityAttribute(String s) {
        return new BigInteger(s);
    }
}
