package ru.artyashkov.controllers;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.artyashkov.dto.ConnectionDto;
import ru.artyashkov.dto.FileMessageDto;
import ru.artyashkov.dto.UserDto;
import ru.artyashkov.services.FilesStorageService;
import ru.artyashkov.services.Service;

import java.math.BigInteger;

@RestController
@RequestMapping
@RequiredArgsConstructor
@Slf4j
public class UserController {
    private final Service service;
    private final FilesStorageService filesStorageService;

    @GetMapping(value = "user/{name}")
    public ResponseEntity<UserDto> findUserByName(@PathVariable String name) {
        return ResponseEntity.ok(service.findUserByName(name));
    }

    @GetMapping(value = "user/{sender}/connection/{receiver}")
    public ResponseEntity<ConnectionDto> findConnectionBySenderAndReceiver(@PathVariable String sender, @PathVariable String receiver) {
        return ResponseEntity.ok(service.findConnectionFromNameToName(sender, receiver));
    }

    @PostMapping(value = "user/{sender}/load/{receiver}", consumes = { "multipart/form-data" })
    public ResponseEntity<FileMessageDto> saveAndEncryptFile(@PathVariable String sender,
                                                             @PathVariable String receiver,
                                                             @RequestBody MultipartFile file) {
        FileMessageDto fileMessageDto = service.saveAndEncryptFile(sender, receiver, file);
        return ResponseEntity.status(HttpStatus.CREATED).body(fileMessageDto);
    }

    @GetMapping(value = "user/{receiver}/files")
    public ResponseEntity<?> getFiles(@PathVariable String receiver) {
        return ResponseEntity.ok(service.getAllMessageByReceiverName(receiver));
    }

    @DeleteMapping(value = "file/{fileName}")
    public ResponseEntity<?> deleteFileByName(@PathVariable String fileName) {
        service.deleteFile(fileName);
        filesStorageService.delete(fileName);
        return ResponseEntity.ok().build();
    }

    @GetMapping("download/{fileName}")
    public ResponseEntity<?> getFileByName(@PathVariable String fileName,
                                           @RequestParam Boolean needDecrypt,
                                           @RequestParam(required = false) String receiver) {
        Pair<String, Resource> file = service.downloadFile(receiver, fileName, needDecrypt);

        HttpHeaders header = new HttpHeaders();
        header.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + file.getLeft());
        header.add("Cache-Control", "no-cache, no-store, must-revalidate");
        header.add("Pragma", "no-cache");
        header.add("Expires", "0");

        return ResponseEntity.ok()
                .headers(header)
                .contentType(MediaType.parseMediaType("application/octet-stream"))
                .body(file.getRight());
    }
}
