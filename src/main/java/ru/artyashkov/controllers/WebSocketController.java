package ru.artyashkov.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import ru.artyashkov.dto.FileMessageDto;

@Controller
@RequiredArgsConstructor
public class WebSocketController {

    private final SimpMessagingTemplate simpMessagingTemplate;

    @MessageMapping("/hello")
    public void handleHello(FileMessageDto message) throws Exception {
        Thread.sleep(1000);
        simpMessagingTemplate.convertAndSend("/topic/greetings/" + message.getReceiverName(), message);
    }
}
