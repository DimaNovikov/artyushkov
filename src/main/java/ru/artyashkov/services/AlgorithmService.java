package ru.artyashkov.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.artyashkov.algorithms.ElGamalRecipient;
import ru.artyashkov.algorithms.ElGamalSender;
import ru.artyashkov.algorithms.RC6Algorithm;
import ru.artyashkov.algorithms.model.EncryptedSecretKey;
import ru.artyashkov.algorithms.model.Keys;
import ru.artyashkov.mapper.EncryptedSecretKeyMapper;
import ru.artyashkov.mapper.KeysMapper;
import ru.artyashkov.model.EncryptedSecretKeyEntity;
import ru.artyashkov.model.KeysEntity;
import ru.artyashkov.model.User;

import java.math.BigInteger;

@Component
@RequiredArgsConstructor
public class AlgorithmService {
    private final KeysMapper keysMapper;
    private final EncryptedSecretKeyMapper encryptedSecretKeyMapper;

    public KeysEntity createKeys() {
        Keys keys = new Keys();
        keys.init();
        return keysMapper.toKeysEntity(keys);

    }

    public EncryptedSecretKeyEntity encryptSecretKey(User recipient) {
        Keys recipientKeys = keysMapper.toKeys(recipient.getKeys());

        ElGamalSender sender = new ElGamalSender(recipientKeys);
        RC6Algorithm encryptMachine = new RC6Algorithm();
        encryptMachine.generateSecretKey();
        EncryptedSecretKey encryptedSecretKey = sender.encryptSecretKey(encryptMachine.getSecretKey());

        return encryptedSecretKeyMapper.toKeyEntity(encryptedSecretKey);
    }

    // запрашиваем ключи пользователя, которому отправляем секрет
    public BigInteger decryptSecretKey(User recipient, EncryptedSecretKeyEntity keyEntity) {
        EncryptedSecretKey key = encryptedSecretKeyMapper.toKey(keyEntity);
        Keys recipientKeys = keysMapper.toKeys(recipient.getKeys());

        ElGamalRecipient decrypter = new ElGamalRecipient(recipientKeys);
        return decrypter.decryptSecretKey(key);
    }

    public BigInteger decryptSecretKey(User recipient, BigInteger c1, BigInteger c2) {
        return decryptSecretKey(recipient, EncryptedSecretKeyEntity.builder()
                .c1(c1)
                .c2(c2)
                .build());
    }

    public byte[] encryptBytes(byte[] text, BigInteger secretKey) {
        RC6Algorithm encryptMachine = new RC6Algorithm();
        encryptMachine.setSecretKey(secretKey);
        return encryptMachine.encrypt(text);
    }

    public byte[] decryptBytes(byte[] text, BigInteger secretKey) {
        RC6Algorithm decryptBytes = new RC6Algorithm();
        decryptBytes.setSecretKey(secretKey);
        return decryptBytes.decrypt(text);
    }


}
