package ru.artyashkov.services;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import ru.artyashkov.dto.ConnectionDto;
import ru.artyashkov.dto.FileMessageDto;
import ru.artyashkov.dto.UserDto;
import ru.artyashkov.exceptions.NotFoundException;
import ru.artyashkov.model.Connection;
import ru.artyashkov.model.EncryptedSecretKeyEntity;
import ru.artyashkov.model.FileMessage;
import ru.artyashkov.model.User;
import ru.artyashkov.repository.ConnectionRepository;
import ru.artyashkov.repository.FileMessageRepository;
import ru.artyashkov.repository.UserRepository;

import javax.transaction.Transactional;
import java.math.BigInteger;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Component
@RequiredArgsConstructor
@Transactional
public class Service {

    private final static DateTimeFormatter FORMAT = DateTimeFormatter.ofPattern("dd-MM-yyyy hh:mm:ss.SSS");

    private final UserRepository userRepository;
    private final ConnectionRepository connectionRepository;
    private final FileMessageRepository fileMessageRepository;

    private final FilesStorageService filesStorageService;
    private final AlgorithmService algorithmService;

    public UserDto findUserByName(String name) {
        User user = getOrCreateUserByName(name);
        return UserDto.builder()
                .name(name)
                .publicKey(user.getKeys().getPublicKey().toString())
                .p(user.getKeys().getP().toString())
                .g(user.getKeys().getG().toString())
                .build();
    }

    public ConnectionDto findConnectionFromNameToName(String senderName, String receiverName) {
        User sender = getOrCreateUserByName(senderName);
        User receiver = getOrCreateUserByName(receiverName);
        EncryptedSecretKeyEntity encryptSecret = getOrCreateConnectionFromUserToUser(sender, receiver).getEncryptSecret();
        return ConnectionDto.builder()
                .c1(encryptSecret.getC1().toString())
                .c2(encryptSecret.getC2().toString())
                .build();
    }

    public FileMessageDto saveAndEncryptFile(String senderName,
                                             String receiverName,
                                             MultipartFile file) {
        User sender = getOrCreateUserByName(senderName);
        User receiver = getOrCreateUserByName(receiverName);
        EncryptedSecretKeyEntity secret = getOrCreateConnectionFromUserToUser(sender, receiver).getEncryptSecret();
        BigInteger secretKey = algorithmService.decryptSecretKey(receiver, secret);

        String fileName = null;
        try {
            byte[] bytes = algorithmService.encryptBytes(file.getBytes(), secretKey);
            fileName = filesStorageService.save(bytes);
        } catch (Exception e) {
            throw new RuntimeException("Не удалось сохранить файл");
        }

        try {
            return saveFileNameIntoDb(sender, receiver, fileName, file.getOriginalFilename());
        } catch (RuntimeException e) {
            log.error(e.getMessage());
            filesStorageService.delete(fileName);
            throw e;
        }
    }
    private FileMessageDto saveFileNameIntoDb(User sender, User receiver, String fileName, String originalFilename) {
        Connection connection = getOrCreateConnectionFromUserToUser(sender, receiver);

        FileMessage file = fileMessageRepository.save(FileMessage.builder()
                .connect(connection)
                .created(LocalDateTime.now())
                .path(fileName)
                .name(originalFilename)
                .build());
        connection.getMessages().add(file);

        return FileMessageDto.builder()
                .id(fileName)
                .receiverName(receiver.getName())
                .name(originalFilename)
                .senderName(sender.getName())
                .created(FORMAT.format(file.getCreated()))
                .c1(connection.getEncryptSecret().getC1().toString())
                .c2(connection.getEncryptSecret().getC2().toString())
                .build();
    }

    public List<FileMessageDto> getAllMessageByReceiverName(String name) {
        List<FileMessageDto> list = new ArrayList<>();
        User receiver = getOrCreateUserByName(name);
        for (Connection c : receiver.getReceiver()) {
            String senderName = c.getSender().getName();
            list.addAll(c.getMessages().stream()
                    .map(message -> FileMessageDto.builder()
                            .id(message.getPath())
                            .name(message.getName())
                            .receiverName(name)
                            .senderName(senderName)
                            .created(FORMAT.format(message.getCreated()))
                            .build())
                    .toList());
        }
        return list;
    }

    public void deleteFile(String fileName) {
        FileMessage file = getFileOrThrowNotFound(fileName);
        fileMessageRepository.delete(file);
    }

    public Pair<String, Resource> downloadFile(String receiverName,
                                               String fileName,
                                               Boolean needDecrypt) {
        FileMessage fileMessage = getFileOrThrowNotFound(fileName);
        byte[] bytes = filesStorageService.load2(fileMessage.getPath());
        if (needDecrypt) {
            User receiver = getOrCreateUserByName(receiverName);
            BigInteger secretKey = algorithmService.decryptSecretKey(receiver, fileMessage.getConnect().getEncryptSecret());
            bytes = algorithmService.decryptBytes(bytes, secretKey);
        }
        return Pair.of(fileMessage.getName(), new ByteArrayResource(bytes, fileMessage.getName()));
    }
    public String getFileName(String fileName) {
        FileMessage file = getFileOrThrowNotFound(fileName);
        return file.getName();
    }

    private FileMessage getFileOrThrowNotFound(String fileName) {
        FileMessage file = fileMessageRepository.findByPath(fileName);
        if (file == null) {
            throw new NotFoundException("файл " + fileName +" не найден");
        }
        return file;
    }

    private User getOrCreateUserByName(String name) {
        User user = userRepository.findByName(name);
        if (user == null) {
            user = User.builder()
                    .name(name)
                    .keys(algorithmService.createKeys())
                    .build();
            userRepository.save(user);
        }
        return user;
    }


    private Connection getOrCreateConnectionFromUserToUser(User sender, User receiver) {
        Connection connection = connectionRepository.findBySenderAndReceiver(sender, receiver);

        if (connection == null) {
            connection = connectionRepository.save(Connection.builder()
                    .sender(sender)
                    .receiver(receiver)
                    .encryptSecret(algorithmService.encryptSecretKey(receiver))
                    .build());
            sender.getSender().add(connection);
            receiver.getReceiver().add(connection);
        }
        return connection;
    }
}
