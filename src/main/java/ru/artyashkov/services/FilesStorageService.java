package ru.artyashkov.services;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Component;
import org.springframework.util.FileCopyUtils;
import org.springframework.util.FileSystemUtils;
import ru.artyashkov.exceptions.BadRequestException;
import ru.artyashkov.exceptions.NotFoundException;
import ru.artyashkov.exceptions.UnauthorizedException;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class FilesStorageService {

    @Value("${files_directory}")
    private String path;
    private Path root;
    private final AlgorithmService service;


    @PostConstruct
    public void init() {
        try {
            root = Paths.get(path);
            if (!root.toFile().exists()) {
                root = Files.createDirectories(root);
            }
        } catch (IOException e) {
            throw new RuntimeException("Could not initialize folder for upload!");
        }
    }

    public String save(byte[] bytes) {
        try {
            String path = UUID.randomUUID().toString();
            FileCopyUtils.copy(bytes, root.resolve(path).toFile());
            return path;
        } catch (FileAlreadyExistsException ex) {
            throw new BadRequestException("A file of that name already exists.");
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    public Resource load(String filename) {
        try {
            Path file = root.resolve(filename);

            //расшифровываем файл

            Resource resource = new UrlResource(file.toUri());

            if (!resource.exists()) {
                throw new NotFoundException("File not found");
            } else if (!resource.isReadable()) {
                throw new UnauthorizedException("Could not read the file");
            } else {
                return resource;
            }
        } catch (Exception e) {
            throw new RuntimeException("Error: " + e.getMessage());
        }
    }

    public byte[] load2(String filename) {
        try {
            Path file = root.resolve(filename);
            return Files.readAllBytes(file);
        } catch (Exception e) {
            throw new RuntimeException("Error: " + e.getMessage());
        }
    }

    public List<String> getFileNames() {
        try {
            return Files.list(root)
                    .map(path -> path.toFile().getName())
                    .filter(name -> !name.equals(".") && !name.equals(".."))
                    .collect(Collectors.toList());
        } catch (Exception e) {
            throw new RuntimeException("Error: " + e.getMessage());
        }
    }

    public void delete(String filename) {
        Path file = root.resolve(filename);
        try {
            if (!Files.exists(file)) {
                throw new NotFoundException("File not found");
            }
            Files.delete(file);
        } catch (IOException e) {
            throw new RuntimeException("Error: " + e.getMessage());
        }
    }

    public void deleteAll() {
        FileSystemUtils.deleteRecursively(root.toFile());
    }
}
