
var domain1 = "http://localhost:8081";
 var domain2 = "http://gachi-huyachi.fun";
 var domain3 = "https://gachi-huyachi.fun";
var wsdomain1 = "ws://localhost:8081";
 var wsdomain2 = "ws://gachi-huyachi.fun";
 var wsdomain3 = "wss://gachi-huyachi.fun";

function getDomain() {
    var currentURL = window.location.href;
    console.log(currentURL);
    if (currentURL.startsWith(domain1)) {
        return domain1
    }
    if (currentURL.startsWith(domain2)) {
        return domain2
    }
    return domain3;
}

function getWsDomain() {
    var currentURL = window.location.href;
    console.log(currentURL);
    if (currentURL.startsWith(domain1)) {
        return wsdomain1
    }
    if (currentURL.startsWith(domain2)) {
        return wsdomain2
    }
    return wsdomain3;
}


// Получить элемент по id
function getId(id) {
    return document.getElementById(id);
}

function request(method, url, data, onSuccess) {
   $.ajax({
   type: method,
   url: url,
   headers: {
       "content-type": "application/json"
   },
   data: data,
   success: onSuccess,
    error: function(response) {
        console.log(response);
        alert(response);
    }
});
}


function loadFile(url, data, onSuccess) {
   $.ajax({
   type: "POST",
   url: url,
   processData: false,
   contentType: false,
   data: data,
   success: onSuccess,
    error: function(response) {
        console.log(response);
        alert(response);
    }
});
}

var senderName = "";
var senderPublicKey = "";
var receiverName = "";
var receiverPublicKey = "";
var c1 = "";
var c2 = "";

function fillBlock1() {
    request("GET", getDomain() + "/user/" + getId("senderName").value, null, function(result) {
        getId("senderPublicKey").textContent = result.publicKey;
        getId("senderP").textContent = result.p;
        getId("senderG").textContent = result.g;
        getId("block1hidden").hidden = false;
        getId("block2").hidden = false;
        getId("fileTable").hidden = false;

        senderName = result.name;
        senderPublicKey = result.publicKey;
        localStorage.setItem("senderName", senderName);

        getMyFileNames();
        connect();
    });
}

function fillBlock2() {
    request("GET", getDomain() + "/user/" + getId("receiverName").value, null, function(result) {
        getId("receiverPublicKey").textContent = result.publicKey;
        getId("receiverP").textContent = result.p;
        getId("receiverG").textContent = result.g;
        getId("block2hidden").hidden = false;

        receiverName = result.name;
        localStorage.setItem("receiverName", receiverName);
        receiverPublicKey = result.publicKey;
        request("GET", getDomain() + "/user/" + senderName + "/connection/" + receiverName, null, function(result) {
            getId("c1").textContent = result.c1;
            getId("c2").textContent = result.c2;
            getId("block3").hidden = false;

            c1 = result.c1;
            c2 = result.c2;
        });
    });
}

function sendFile() {
    var data = new FormData($("#form1")[0]);
    loadFile(getDomain() + "/user/" + senderName + "/load/" + receiverName, data, function(result) {
        $('#file1').val('');

        sendFileToReceiver(result);
    })
}

function getMyFileNames() {
    request("GET", getDomain() + "/user/" + senderName + "/files", null,
        function(response) {
            $("#greetings").html("");
            $.each(response, function(index, title) {
                createFileLine(response[index]);
            })
        })
}

function createFileLine(response) {

   var tr = document.createElement('tr');
   tr.id = response.id;
   $("#greetings").append(tr);
   var elem = document.createElement('td');
   elem.textContent = response.name + " " + response.created;
   tr.appendChild(elem);

   var downloadFile = document.createElement('button');
   downloadFile.textContent = "download";
   elem.appendChild(downloadFile);
   downloadFile.onclick = function() {
      downloadFileByUrl(response.name, "/download/" + response.id  + "?needDecrypt=" + false);
   }

    var downloadFile = document.createElement('button');
    downloadFile.textContent = "downloadAndDecrypt";
    elem.appendChild(downloadFile);
    downloadFile.onclick = function() {
       downloadFileByUrl(response.name, "/download/" + response.id + "?needDecrypt=" + true +"&receiver=" + response.receiverName);
    }

   var deleteFile = document.createElement('button');
   deleteFile.textContent = "delete";
   elem.appendChild(deleteFile);
   deleteFile.onclick = function() {
      deleteFileByUrl("/file/" + response.id);
      const element = document.getElementById(response.id);
      if (element) {
        element.parentNode.removeChild(element);
      }
   }
}

function downloadFileByUrl(fileName, fileUrl) {
    request("GET", getDomain() + fileUrl, null, function(result) {
            let blob = new Blob([result], { type: "application/octetstream" });

            let a = document.createElement('a');
            a.href = window.URL.createObjectURL(blob);
            a.download = fileName;
            document.body.appendChild(a);
            a.click();
            document.body.removeChild(a);
            window.URL.revokeObjectURL(a.href);
        }
    );
}

function deleteFileByUrl(fileUrl) {
    request("DELETE", getDomain() + fileUrl, null, null);
}

const stompClient = new StompJs.Client({
    brokerURL: getWsDomain() + "/gs-guide-websocket"
});

stompClient.onConnect = (frame) => {
    console.log('Connected: ' + frame);
    stompClient.subscribe('/topic/greetings/' + senderName, (greeting) => {
        console.log(JSON.parse(greeting.body));
        createFileLine(JSON.parse(greeting.body));
    });
};

stompClient.onWebSocketError = (error) => {
    console.error('Error with websocket', error);
    disconnect();
};

stompClient.onStompError = (frame) => {
    console.error('Broker reported error: ' + frame.headers['message']);
    console.error('Additional details: ' + frame.body);
};

function connect() {
    stompClient.activate();
}

function disconnect() {
    stompClient.deactivate();
    console.log("Disconnected");
}

function sendFileToReceiver(response) {
    stompClient.publish({
        destination: "/app/hello",
        body: JSON.stringify(response)
    });
}

$(function () {
    $("form").on('submit', (e) => e.preventDefault());
    $( "#connect" ).click(() => fillBlock1());
    $( "#getReceiver" ).click(() => fillBlock2());
    $( "#send" ).click(() => sendFile());
});

window.onbeforeunload = function() {
    disconnect();
    return null;
};

window.onload = function() {
    if (localStorage.getItem("senderName") != null) {
        $('#senderName').val(localStorage.getItem("senderName"));
        if (localStorage.getItem("receiverName") != null) {
            $('#receiverName').val(localStorage.getItem("receiverName"));
        }
    }
}